import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { AppService } from './app.service';

describe('AppService', () => {
  let httpMock: HttpTestingController;
  let appService: AppService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AppService],
    });

    appService = TestBed.get(AppService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('getStores() should http GET stores', () => {
    const stores = [
      {
        type: 'StoreListRO',
        uuid: 'gmcKYx4X5HEAAAFIdhIYwKxK',
        addressName: 'Jumbo Aalsmeer Ophelialaan.',
        street: 'Ophelialaan',
        street2: '124',
        street3: '',
        city: '',
        postalCode: '',
        distance: 0,
        todayOpen: '08:00',
        todayClose: '22:00',
        latitude: '52.264417',
        longitude: '4.762433',
        locationType: 'SupermarktPuP',
        newStore: 0,
        collectionPoint: true,
        complexNumber: '33010',
        sapStoreID: '3629',
        favourite: false,
        isHomeStore: false,
        showWarningMessage: true,
        sundayOpen: true,
      },
    ];

    appService.getStores().subscribe((res) => {
      expect(res).toEqual(stores);
    });

    const req = httpMock.expectOne(
      'https://api.jsonstorage.net/v1/json/00000000-0000-0000-0000-000000000000/c4357a15-46e2-4542-8e93-6aa6a0c33c1e'
    );
    expect(req.request.method).toEqual('GET');
    req.flush(stores);

    httpMock.verify();
  });
});

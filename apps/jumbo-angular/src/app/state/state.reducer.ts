import { createReducer, on } from '@ngrx/store';

import { StoreModel } from './model';
import { addStores } from './state.actions';

export const initialState: StoreModel[] = [];

export const storeReducer = createReducer(
  initialState,
  on(addStores, (_, payload) => payload.stores)
);

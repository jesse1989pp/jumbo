import { createSelector } from '@ngrx/store';
import { StoreModel, AppState } from './model';

export const selectStores = (state: AppState) => state.stores;

export const selectStoreCities = createSelector(
  selectStores,
  (stores: StoreModel[]) => [...new Set(stores.map((store) => store.city))]
);

import { AppState, StoreModel } from './model';
import { selectStoreCities } from './state.selectors';

describe('Selectors', () => {
  const initialState: AppState = {
    stores: [
      {
        city: 'Aalsmeer',
      },
      {
        city: 'Veghel',
      },
      {
        city: 'Aalsmeer',
      },
    ] as StoreModel[],
  };

  it('should select the filterd cities list of the stores', () => {
    const result = selectStoreCities.projector(initialState.stores);
    expect(result.length).toEqual(2);
    expect(result[0]).toEqual('Aalsmeer');
  });
});

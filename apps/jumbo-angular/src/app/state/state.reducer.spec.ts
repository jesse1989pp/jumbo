import { StoreModel } from './model';
import { addStores } from './state.actions';
import * as reducer from './state.reducer';
describe('storeReducer', () => {

  describe('addStores', () => {
    it('should retrieve all stores added', () => {
      const { initialState } = reducer;
      const newStores = [
        {
          city: 'Veghel',
        }
      ] as StoreModel[];
      const action = addStores({ stores: newStores });
      const state = reducer.storeReducer(initialState, action);

      expect(state).toEqual(newStores);
      expect(state).not.toBe(initialState);
    });
  });
});

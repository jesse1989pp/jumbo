import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { exhaustMap, map } from 'rxjs';

import { AppService } from '../app.service';
import { addStores, getStores } from './state.actions';

@Injectable()
export class StoreEffects {
  constructor(
    private actions$: Actions,
    private appService: AppService
  ) {}

  getStores$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getStores),
      exhaustMap(() =>
        this.appService
          .getStores()
          .pipe(map((payload) => addStores({ stores: payload })))
      )
    )
  );
}

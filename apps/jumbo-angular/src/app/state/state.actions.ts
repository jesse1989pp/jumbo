import { createAction, props } from '@ngrx/store';
import { StoreModel } from './model';

export const getStores = createAction('[Stores] Get stores from API');
export const addStores = createAction(
  '[Stores] Add stores to store/state',
  props<{ stores: StoreModel[] }>()
);

import { getStores } from './state/state.actions';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject, combineLatest, map } from 'rxjs';
import { StoreModel } from './state/model';
import { selectStores, selectStoreCities } from './state/state.selectors';

@Component({
  selector: 'jumbo-angular-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  stores$: Observable<StoreModel[]>;
  cities$: Observable<string[]>;
  selectedCity = new BehaviorSubject<string>('');

  constructor(private store: Store<{ stores: StoreModel[] }>) {
    this.stores$ = combineLatest([
      this.store.select(selectStores),
      this.selectedCity,
    ]).pipe(
      map(([stores, selectedCity]) => {
        const filteredResults = stores.filter(
          (store) => store.city.toLowerCase() === selectedCity.toLowerCase()
        );
        return filteredResults.length > 0 ? filteredResults : stores;
      })
    );
    this.cities$ = store.select(selectStoreCities);
  }

  ngOnInit(): void {
    this.store.dispatch(getStores());
  }

  onStoreChange(event: Event): void {
    const input = event.target as HTMLInputElement;
    this.selectedCity.next(input.value);
  }
}

import { StoreModel } from '../state/model';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'jumbo-angular-store-item',
  templateUrl: './store-item.component.html',
  styleUrls: ['./store-item.component.scss'],
})
export class StoreItemComponent {
  @Input() store: StoreModel = {} as StoreModel;
}

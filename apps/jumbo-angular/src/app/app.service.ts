import { StoreModel } from './state/model';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient,  } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  jumboEndpoint = 'https://api.jsonstorage.net/v1/json/00000000-0000-0000-0000-000000000000/c4357a15-46e2-4542-8e93-6aa6a0c33c1e';

  constructor(private http: HttpClient) {}


  getStores(): Observable<StoreModel[]> {
    return this.http.get<StoreModel[]>(this.jumboEndpoint);
  }
}

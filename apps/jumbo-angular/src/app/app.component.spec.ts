import { getStores } from './state/state.actions';
import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { AppComponent } from './app.component';
import { AppState } from './state/model';
import { StoreItemComponent } from './store-item/store-item.component';
import { StoreListComponent } from './store-list/store-list.component';
import { skip, take } from 'rxjs';
describe('AppComponent', () => {
  let store: MockStore;
  const initialState: AppState = { stores: [] };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent, StoreItemComponent, StoreListComponent],
      providers: [provideMockStore({ initialState })],
    }).compileComponents();

    store = TestBed.inject(MockStore);
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should render logo & filter', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('header img')?.DOCUMENT_NODE).toBeGreaterThan(
      0
    );
    expect(
      compiled.querySelector('header .store-filter')?.DOCUMENT_NODE
    ).toBeGreaterThan(0);
  });

  describe('OnInit', () => {
    it('should get all stores', () => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.componentInstance;
      jest.spyOn(store, 'dispatch');
      app.ngOnInit();
      expect(store.dispatch).toHaveBeenCalledWith(getStores());
    });
  });

  describe('onStoreChange', () => {
    it('should update the selectedCity', async () => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.componentInstance;
      app.selectedCity.pipe(skip(1), take(1)).subscribe((city) => {
        expect(city).toBe('Veghel');
      });

      app.onStoreChange({ target: { value: 'a' } } as unknown as Event);
    });
  });
});

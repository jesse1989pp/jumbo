import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'formatAddressName' })
export class FormatAddressNamePipe implements PipeTransform {
  transform(value: string): string {
    return value.replace('Jumbo ', '');
  }
}

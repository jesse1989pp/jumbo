import { Component, Input } from '@angular/core';
import { StoreModel } from '../state/model';

@Component({
  selector: 'jumbo-angular-store-list',
  templateUrl: './store-list.component.html',
  styleUrls: ['./store-list.component.scss'],
})
export class StoreListComponent {
  @Input() stores: StoreModel[] = [];
}

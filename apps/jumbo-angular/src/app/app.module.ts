import { FormatAddressNamePipe } from './address-name.pipe';
import { StoreItemComponent } from './store-item/store-item.component';
import { HttpClientModule } from '@angular/common/http';
import { StoreEffects } from './state/state.effects';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { StoreListComponent } from './store-list/store-list.component';
import { storeReducer } from './state/state.reducer';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [
    AppComponent,
    FormatAddressNamePipe,
    StoreItemComponent,
    StoreListComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    StoreModule.forRoot({ stores: storeReducer }),
    EffectsModule.forRoot([StoreEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

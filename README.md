

# JumboAngular

Used the NX build system to set up a quick angular environment. I wanted to test the application with Jest, have same basic linting etc. Some of the structure i've kept in the project, but was not edited.(e2e for example). All changes for this assignment are within `apps/jumbo-angular`. Application can be ran with the below explanation. I added the original vue assignement at the bottom aswell. I was told an angular implementation would do at this point.

## Install
Run `npm install` in the root to set up the project.

## Development server
Run `npm run serve ` for a dev server. Navigate to http://localhost:4200/. 

## Running unit tests
Run `npm run test` to execute the unit tests via [Jest](https://jestjs.io).

# Jumbo Vue/Angular technical assignment

Requirements for this assignment:

x Get the data from jsonstorage.net (see details below)
x Use an action and mutation to save the data
x Add multiple Vuex getters
x Prevent putting everything in actions by using a service
x Keep components simple and stupid (logic belongs in the store)
x Add unit testing for the store, service and components
x Replace the hardcoded cities list with a dynamic one (using the given data)
x Introduce an input field that filters the stores based on: addressName or cityname

Feel free to add anything!

## Data

You can get the data from this endpoint:
https://api.jsonstorage.net/v1/json/00000000-0000-0000-0000-000000000000/c4357a15-46e2-4542-8e93-6aa6a0c33c1e

## Bonus points

~ Make it look like it's a Jumbo application (styling)
- Create a new jsonstorage.net entity and use it as your last searched for database
x Chop off the prefix `Jumbo` from the addressName
- Create a new view for stores per city
